import React from "react";
import Comp1 from "./componentes/comp1";
import styled from "styled-components";
import Consulta from "./componentes/consulta";


const ContenedorPadre = styled.div`
    width: 100%;
    height: 100vh;
    margin: auto;
    background-color: lightblue;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

`
const Pais = styled.div`
  width: 200px;
  height: 200px;
  border-radius: 10px;
  background-color: white;
  border: 3px solid green;
  display: flex;
  flex-direction: column;
  align-items: center;
`
const Input = styled.div`
  width: 350px;
  height: 200px;
  display: flex;
  justify-content: center;
`


function App() {
  return (
    
    <ContenedorPadre>

      <Comp1 />

      <Pais>
        <h1 id="pais_name"></h1>
        <img id="pais_flag" style={{width: '100px'}} alt="bandera"/>
        <p id="pais_region"></p>
      </Pais>
      <Input>
        <div className="row">
          <div className="col-md-6">
            <input type={'text'} id='pais_input' className="form-control" placeholder="Pais.." />
          </div>
          <div className="col-md-6">
            <button id="btn_search" className="btn btn-primary">Buscar</button>
          </div>
        </div>
      </Input>
      
      <Consulta/>
      
    </ContenedorPadre>
    
  );
};

export default App;
