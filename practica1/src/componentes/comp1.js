import React from 'react';
import styled from 'styled-components';



const Contenedor = styled.div`
    width: 70vh;
    height: 20vh;
    background: #000;
    border: 1px solid white;
    border-radius: 15px;
    margin: 100px auto;

`

const Titulo = styled.div`
    font-family: open-sans;
    font-weight: bolder;
    font-size: 15px;
    text-decoration: underline;
    color: white;
    text-align: center;
    margin: auto;
`


const comp1 = () => {
  return (
    <Contenedor>
        <Titulo>
            <h1>Soy un componente importado</h1>
            <p>estoy en la carpeta componentes, el de abajo tambien es un componente, consume una API.</p>
        </Titulo>
    </Contenedor>
  )
}

export default comp1