import React from 'react'
import styled from 'styled-components'


const Contenedor = styled.div`
    width: 200px;
    height: 200px;
    border: 3px double white;
    background-color: gray;
    border-radius: 10%;
    margin-bottom: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
`

const Button = styled.button`
    width: 100px;
    height: 50px;
    background: orange;
    color: white;
    font-weight: bolder;
    border-radius: 10px;
    text-align: center;
`

class country {
  constructor ( pais ) {
    this.flag = pais.flag;
    this.name = pais.name;
    this.region = pais.region;
    this.subregion = pais.subregion;
  }
};

const getCountry = async ( name ) =>{
  const uri = `https://restcountries.com/v2/name/${name}`;
  const resp = await fetch(uri);
  const data = await resp.json()

  let pais = new country (data[0]);
  
  document.getElementById('pais_name').innerHTML = pais.name;
  document.getElementById('pais_flag').src = pais.flag;
  document.getElementById('pais_region').innerHTML = pais.region;
  
}

const $btn_search = document.getElementById('btn_search'); 
$btn_search.addEventListener('click', ()=>{
  const $pais_input = document.getElementById('pais_input');
  getCountry($pais_input.value);
});

const consulta = () => {
  return (

    <Contenedor>
        <Button> 
            Presioname
        </Button>
    </Contenedor>
  )
}

export default consulta