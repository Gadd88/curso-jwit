import React from 'react'
import styled from 'styled-components'

const Title = styled.h2`
    color: #7e7e7e;
    font-weight: 500;
`
const Button = styled.button`
    width: 169px;
    height: 70px;
    background-color: #eeeeee;
    border-radius: 15px;
    border: 2px solid #eeeeee;
    margin: 3px;
    &:hover, &:active {
        border: 2px solid #6495ed;
    }
    font-weight: bolder;
    font-size: 15px;
    cursor: pointer;
    

`
const Sale = () => {
  return (
    <div>
        <Title>Sale Type</Title>
        <Button>DAI</Button>
        <Button>WETH</Button>
        <Button>ETH</Button>
        <Button>USDC</Button>
    </div>
  )
}

export default Sale