import React from 'react'
import styled from 'styled-components'
import '../App.css'

const Container = styled.div`
    width: 100%;
    height: 220px;

`

const Title = styled.h2`
    color: #7e7e7e;
    font-weight: 500;
`

const InnerContent = styled.div`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    align-items: center;

`
const Text = styled.p`
    color: #aaaaaa;
    font-weight: 600;
    font-size: 20px;
`
const Pricing = () => {
  return (
    <Container>
        <Title>Pricing range</Title>
        <InnerContent>
            <select className='coin'>
                <option>Ethereum</option>
                <option>Solana</option>
                <option>Bitcoin</option>
                <option>Cardano</option>
                <option>Trezos</option>
            </select>
            <input className='price' />
            <Text>To</Text>
            <input className='price' />
            <input id="typeinp" type="range" min="0" max="5" defaultValue="3" step="1"/>

        </InnerContent>
    </Container>
  )
}

export default Pricing