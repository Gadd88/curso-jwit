import React from 'react'
import bitcoin from '../img/btc.png'
import cardano from '../img/cardano.png'
import eth from '../img/eth.png'
import solana from '../img/sol.png'
import tezos from '../img/tezos.png'
import styled from 'styled-components'


const Container = styled.div`
    width: 100%;
    height: 300px;
    background-color: #fff;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    
`
const ImgCont = styled.div`
    width: 120px;
    height: 25px;
    border-radius: 15px;
    border: 2px solid #eeeeee;
    background-color: #eeeeee;
    padding: 20px 20px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    margin: 5px 5px;
    cursor: pointer;
    &:hover, &:active {
        border: 2px solid #6495ed;
    }
    
`
const styleImg = {
    width: '35px',
    height: '35px',
}

const Title = styled.h2`
    color: #7e7e7e;
    font-weight: 500;

`
const ContainerLogos = styled.div`
    display: flex;
    flex-wrap: wrap;

`

const Title2 = styled.h3`
    font-weight: 700;
    color: #000
    font-family: open-sans;
    padding-left: 5px;
`


const Logos = () => {
  return (
    <Container>
        <Title>Blockchain</Title>
        <ContainerLogos>
            <ImgCont>
                <img src={bitcoin} style={styleImg} alt="Btc Logo"/>
                <Title2>Bitcoin</Title2>           
            </ImgCont>
            <ImgCont>
                <img src={eth} style={styleImg} alt="eth logo" />
                <Title2>Ethereum</Title2>
            </ImgCont>
            <ImgCont>
                <img src={solana} style={styleImg} alt="solana logo" /> 
                <Title2>Solana</Title2>
            </ImgCont>
            <ImgCont>
                <img src={cardano} style={styleImg} alt="cardano logo" />
                <Title2>Cardano</Title2>
            </ImgCont>
            <ImgCont>
                <img src={tezos} style={styleImg} alt="tezos logo" />
                <Title2>Tezos</Title2>
            </ImgCont>
        </ContainerLogos>        
    </Container>
)}

export default Logos