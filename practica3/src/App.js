import './App.css';
import styled from 'styled-components';
import Logos from './componentes/blockchain';
import Sale from './componentes/sale';
import Pricing from './componentes/pricing';

const Container = styled.div`
  width: 350px;
  height: 80vh;
  border-radius: 15px;
  border: 1px solid #EDEDED;
  background-color: #fff;
  margin: 10px auto;
  padding: 20px;
`


function App() {
  return (
    <>
      <Container>
        <Pricing />
        <Logos />
        <Sale />
      </Container>      
    </>
  );
}

export default App;
