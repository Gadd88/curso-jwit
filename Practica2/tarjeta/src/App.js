import React, { useState } from 'react';
import styled from 'styled-components';
import Card from './componentes/card'
import Submenu from './componentes/submenu'

const Container = styled.div`

`;


const App = () => {

  const [data, setData] = useState([
    { 
      id: 1,
      color:"#21d0d0",
      options: [
        {title: "PRICE LOW TO HIGH", state: false},
        {title: "PRICE HIGH TO LOW", state: false},
        {title: "popularity", state: true},
        ],
        footer: 1
    },
    { 
      id: 2,
      color:"#ff7745",
      options: [
        {title: "1up nutrition", state: false},
        {title: "asitis", state: false},
        {title: "aavatar", state: false},
        {title: "big muscles", state: false},
        {title: "bpi sports", state: false},
        {title: "bsn", state: false},
        {title: "cellucor", state: false},
        {title: "domini8r", state: false},
        {title: "dymatize", state: false},
        {title: "dynamik", state: false},
        {title: "esn", state: false},
        {title: "evlution nutrition", state: false},
        ],
        footer: 2
    },
  ]);
  
  const handleChange = (value, id) =>{
    
    const match = data.filter(elem => elem.id === id)[0];

    const actual = match.options.filter(elem => elem.title === value)[0];

    const copy = [...match.options];

    //find index of item to be replaced
    const targetIndex = copy.findIndex(f => f.title === value);

    if (targetIndex > -1){
      //replace the object with a new one
      copy[targetIndex] = { title: value, state: !actual.state};

      match.options = copy;
      
    };

    const copyList = [...data];

    const targetIndexlist = copyList.findIndex(f => f.title === id);

    if (targetIndexlist > -1) {

      copy[targetIndexlist] = match;

    }

    setData(copyList)

  }

  return (

    <Container>

      <Card />

      {data.map((v, i) => <Submenu {...v} onChange={handleChange} />)}
      
    </Container>
    
  );
}

export default App;
