import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
    height: 150px;
    width: 350px;
    border-radius: 15px;
    background-color: #fff0ea;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 10px;
    opacity: 0.8;
`;

const Title = styled.p`
    color: #f06f37;
    text-transform: uppercase;
    font-size: 14px;
    font-family: open sans;
    font-weight: bolder;
    font-style: italic;
    width: 250px;
    text-align: center;
`;

const Button = styled.button`
    background-color: #f06f37;
    text-transform: uppercase;
    border-radius: 20px;
    padding: 5px 20px;
    border: none;
    font-family: open sans;
    font-weight: bolder;
    color: #fff;
    font-style: italic;
    cursor: pointer;
    box-shadow: 1px 1px 5px #f06f37;
`;

const App = () => {
    return(
        <Container>
            <Title>
                use code: pigi100 for rs.100 on your first order!
            </Title>
            <Button>
                grab now
            </Button>

        </Container>
    );
};

export default App;