import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import check from '../img/check.svg';

const Container = styled.div`
    margin: 15px 25px 0 25px;
    display: flex;
    justify-content: space-between;
    align-items: center;    
`;

const Title = styled.p`
    color: #fff;
    font-weight: bolder;
    font-family: open sans;
    text-transform: uppercase;
    font-size: 12px;
    
`;

const Circulo = styled.div`
    border-radius: 50%;
    border: 1.8px solid #fff;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${props => props.visible ? 'white' : 'transparent'};
    
`;

const Icon = styled.img`
    height: 35px;
    width: 35px;
    visibility: ${props => props.visible ? 'visible' : 'hidden'};
`;


const App = ({title, state, onChange}) =>{

    const [visible, setVisible] = useState(false);

    useEffect = (() =>{
        if(state) setVisible(true); //podria tambien poner solo setVisible(state) en vez de if/else y funcionaria.
        else setVisible(false)
    }, [state]);

    const handleChange = () =>{
        setVisible(!visible)
        onChange(!visible)
    };

    return(

        <Container>

            <Title>
                {title}
            </Title>
            <Circulo visible={visible} onClick={handleChange}>
                <Icon src={check} visible={visible} />
            </Circulo>

        </Container>


    )
};

export {Circulo};
export {Icon};

export default App;
