import React from 'react';
import styled from 'styled-components';
import Options from './options';
import Footer from './footer';

const Container = styled.div`
    background-color: ${props => props.color};
    border-radius: 15px;
    padding: 10px;
    width: 380px;
    opacity: 0.8;
    margin: 10px;
    display: flex;
    flex-direction: column;
    
`;


const App = (params) =>{

    // const options = [
    //     {title: "PRICE LOW TO HIGH", state: false},
    //     {title: "PRICE HIGH TO LOW", state: false},
    //     {title: "popularity", state: true},
    // ];
    const handleChange = (value) => {
        if (typeof params.onChange ==='function') params.onChange(value, params.id)
    }

    const handleReset = () =>{
        if (typeof params.onReset ==='function') params.onReset(params.id)
    }

    return(

        <Container color={params.color}>
            
            {params.options.map((v, i) => <Options key={i} {...v} onChange={handleChange}/> )}

            <Footer version={params.footer} onReset={handleReset}/*onReset onCancel onAccept*/ />

        </Container>

        

    )
}

export default App;