import React, { useState } from 'react';
import styled from 'styled-components';
import {Circulo, Icon} from './options';
import cancel from '../img/x.svg'
import check from '../img/check.svg'
import reset from '../img/reset.svg'

const Container = styled.div`
    background-color: ${props => props.color};
    border-radius: 15px;
    padding: 10px;
    width: 380px;
    margin: 10px auto;
    display: flex;
    justify-content: center;
    
`;

const ContainerV2 = styled.div`
    background-color: ${props => props.color};
    border-radius: 15px;
    padding: 10px;
    width: 380px;
    margin: 10px auto;
    display: flex;
    justify-content: center;
    margin-top: 15px;
    padding: 15px 0;
    border-top: 1px solid #ffffff80;
`;

const App = ({ version, onReset, onCancel, onAccept }) =>{

    let circuloStyle={
        boxShadow: '1px 2px 2px rgba(0,0,0,0.5)',
        height: '30px',
        width: '30px',
        padding: '10px',
        margin: '0 5px',
        border:'1px #fff',
    }

    let iconStyle={
        height: '30px',
        width: '30px',
    }

    const handleReset = () => {
        if (typeof onReset ==='function') onReset()
    }

    const handleOk = () => {
        if (typeof onAccept === 'function') onAccept()
    }

    const handleCancel = () => {
        if (typeof onCancel === 'function') onCancel()
    }

    const Item = ({ v, onClick }) => {
        return (
            <Circulo visible = {true} style={circuloStyle} onClick={onClick}>
                <Icon visible = {true} src={v} style={iconStyle}/>
            </Circulo>
        )
    }

    const [component, setComponent] = useState(1);

    if (version === 1) {

        let data = [
            { icon: cancel, onClick: handleCancel }, 
            { icon: check, onClick: handleOk }
        ]
    

        return(

            <Container>
                    
                    {data.map((v, i) => <Item v={v.icon} onClick={v.onClick} />)}
                    {/* //renderizamos una lista usando .map, de esta forma solo repetimos el Circulo y el icono, aclarando los elementos que se reemplazan en (v,i) */}
            </Container>
        );
    };

    if (version === 2) {

        let data = [
            { icon: cancel, onClick: handleCancel }, 
            { icon: reset, onClick: handleReset },
            { icon: check, onClick: handleOk }
        ]

        return(

            <ContainerV2>
                
                {data.map((v, i) => <Item v={v.icon} onClick={v.onClick} />)}

            </ContainerV2>
        );
    }
};



export default App;